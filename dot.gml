# generated with lemon-gml
graph [
 directed 1
 node [ id 0 label " utf ::= * TOKEN_UTF8BOM
	utf ::= *
	startdot ::= * utf thetype thename startdot2
	startdot ::= *
	 " ]
 node [ id 1 label " ctext ::= * TOKEN_QTEXT
	ctext ::= * ctext TOKEN_PLUS TOKEN_QTEXT
	text ::= * TOKEN_TEXT
	text ::= * TOKEN_NUM
	text ::= * ctext
	statements ::= * statement
	statements ::= * statements statement
	statement ::= * statement2
	statement ::= * statement2 TOKEN_SC
	statement2 ::= * nstatement
	statement2 ::= * estatement
	statement2 ::= * astatement
	statement2 ::= * sstatement
	nstatement ::= * nstatement_nidid oattrib
	nstatement_nidid ::= * nidid
	nidid ::= * text
	nidid ::= * text TOKEN_COLON text
	nidid ::= * text TOKEN_COLON text TOKEN_COLON text
	nid ::= * text
	nid ::= * text TOKEN_COLON text
	nid ::= * text TOKEN_COLON text TOKEN_COLON text
	estatement ::= * estatement_nid estatement_erhs oattrib
	estatement ::= * estatement_sstatement estatement_erhs oattrib
	estatement_nid ::= * nid
	estatement_sstatement ::= * sstatement
	astatement ::= * atype tattr
	astatement ::= * aset
	atype ::= * TOKEN_GRAPH
	atype ::= * TOKEN_NODE
	atype ::= * TOKEN_EDGE
	aset ::= * text TOKEN_IS text
	sstatement ::= * shead TOKEN_BRACEOPEN statements TOKEN_BRACECLOSE
	sstatement ::= * TOKEN_BRACEOPEN sstatement_a statements TOKEN_BRACECLOSE
	sstatement ::= TOKEN_BRACEOPEN sstatement_a * statements TOKEN_BRACECLOSE
	shead ::= * TOKEN_SUBGRAPH text
	 " ]
 node [ id 2 label " ctext ::= * TOKEN_QTEXT
	ctext ::= * ctext TOKEN_PLUS TOKEN_QTEXT
	text ::= * TOKEN_TEXT
	text ::= * TOKEN_NUM
	text ::= * ctext
	statements ::= * statement
	statements ::= * statements statement
	statement ::= * statement2
	statement ::= * statement2 TOKEN_SC
	statement2 ::= * nstatement
	statement2 ::= * estatement
	statement2 ::= * astatement
	statement2 ::= * sstatement
	nstatement ::= * nstatement_nidid oattrib
	nstatement_nidid ::= * nidid
	nidid ::= * text
	nidid ::= * text TOKEN_COLON text
	nidid ::= * text TOKEN_COLON text TOKEN_COLON text
	nid ::= * text
	nid ::= * text TOKEN_COLON text
	nid ::= * text TOKEN_COLON text TOKEN_COLON text
	estatement ::= * estatement_nid estatement_erhs oattrib
	estatement ::= * estatement_sstatement estatement_erhs oattrib
	estatement_nid ::= * nid
	estatement_sstatement ::= * sstatement
	astatement ::= * atype tattr
	astatement ::= * aset
	atype ::= * TOKEN_GRAPH
	atype ::= * TOKEN_NODE
	atype ::= * TOKEN_EDGE
	aset ::= * text TOKEN_IS text
	sstatement ::= * shead TOKEN_BRACEOPEN statements TOKEN_BRACECLOSE
	sstatement ::= shead TOKEN_BRACEOPEN * statements TOKEN_BRACECLOSE
	sstatement ::= * TOKEN_BRACEOPEN sstatement_a statements TOKEN_BRACECLOSE
	shead ::= * TOKEN_SUBGRAPH text
	 " ]
 node [ id 3 label " startdot2 ::= TOKEN_BRACEOPEN * statements TOKEN_BRACECLOSE
	ctext ::= * TOKEN_QTEXT
	ctext ::= * ctext TOKEN_PLUS TOKEN_QTEXT
	text ::= * TOKEN_TEXT
	text ::= * TOKEN_NUM
	text ::= * ctext
	statements ::= * statement
	statements ::= * statements statement
	statement ::= * statement2
	statement ::= * statement2 TOKEN_SC
	statement2 ::= * nstatement
	statement2 ::= * estatement
	statement2 ::= * astatement
	statement2 ::= * sstatement
	nstatement ::= * nstatement_nidid oattrib
	nstatement_nidid ::= * nidid
	nidid ::= * text
	nidid ::= * text TOKEN_COLON text
	nidid ::= * text TOKEN_COLON text TOKEN_COLON text
	nid ::= * text
	nid ::= * text TOKEN_COLON text
	nid ::= * text TOKEN_COLON text TOKEN_COLON text
	estatement ::= * estatement_nid estatement_erhs oattrib
	estatement ::= * estatement_sstatement estatement_erhs oattrib
	estatement_nid ::= * nid
	estatement_sstatement ::= * sstatement
	astatement ::= * atype tattr
	astatement ::= * aset
	atype ::= * TOKEN_GRAPH
	atype ::= * TOKEN_NODE
	atype ::= * TOKEN_EDGE
	aset ::= * text TOKEN_IS text
	sstatement ::= * shead TOKEN_BRACEOPEN statements TOKEN_BRACECLOSE
	sstatement ::= * TOKEN_BRACEOPEN sstatement_a statements TOKEN_BRACECLOSE
	shead ::= * TOKEN_SUBGRAPH text
	 " ]
 node [ id 4 label " ctext ::= * TOKEN_QTEXT
	ctext ::= * ctext TOKEN_PLUS TOKEN_QTEXT
	text ::= * TOKEN_TEXT
	text ::= * TOKEN_NUM
	text ::= * ctext
	statements ::= statements * statement
	statement ::= * statement2
	statement ::= * statement2 TOKEN_SC
	statement2 ::= * nstatement
	statement2 ::= * estatement
	statement2 ::= * astatement
	statement2 ::= * sstatement
	nstatement ::= * nstatement_nidid oattrib
	nstatement_nidid ::= * nidid
	nidid ::= * text
	nidid ::= * text TOKEN_COLON text
	nidid ::= * text TOKEN_COLON text TOKEN_COLON text
	nid ::= * text
	nid ::= * text TOKEN_COLON text
	nid ::= * text TOKEN_COLON text TOKEN_COLON text
	estatement ::= * estatement_nid estatement_erhs oattrib
	estatement ::= * estatement_sstatement estatement_erhs oattrib
	estatement_nid ::= * nid
	estatement_sstatement ::= * sstatement
	astatement ::= * atype tattr
	astatement ::= * aset
	atype ::= * TOKEN_GRAPH
	atype ::= * TOKEN_NODE
	atype ::= * TOKEN_EDGE
	aset ::= * text TOKEN_IS text
	sstatement ::= * shead TOKEN_BRACEOPEN statements TOKEN_BRACECLOSE
	sstatement ::= * TOKEN_BRACEOPEN sstatement_a statements TOKEN_BRACECLOSE
	sstatement ::= TOKEN_BRACEOPEN sstatement_a statements * TOKEN_BRACECLOSE
	shead ::= * TOKEN_SUBGRAPH text
	 " ]
 node [ id 5 label " ctext ::= * TOKEN_QTEXT
	ctext ::= * ctext TOKEN_PLUS TOKEN_QTEXT
	text ::= * TOKEN_TEXT
	text ::= * TOKEN_NUM
	text ::= * ctext
	statements ::= statements * statement
	statement ::= * statement2
	statement ::= * statement2 TOKEN_SC
	statement2 ::= * nstatement
	statement2 ::= * estatement
	statement2 ::= * astatement
	statement2 ::= * sstatement
	nstatement ::= * nstatement_nidid oattrib
	nstatement_nidid ::= * nidid
	nidid ::= * text
	nidid ::= * text TOKEN_COLON text
	nidid ::= * text TOKEN_COLON text TOKEN_COLON text
	nid ::= * text
	nid ::= * text TOKEN_COLON text
	nid ::= * text TOKEN_COLON text TOKEN_COLON text
	estatement ::= * estatement_nid estatement_erhs oattrib
	estatement ::= * estatement_sstatement estatement_erhs oattrib
	estatement_nid ::= * nid
	estatement_sstatement ::= * sstatement
	astatement ::= * atype tattr
	astatement ::= * aset
	atype ::= * TOKEN_GRAPH
	atype ::= * TOKEN_NODE
	atype ::= * TOKEN_EDGE
	aset ::= * text TOKEN_IS text
	sstatement ::= * shead TOKEN_BRACEOPEN statements TOKEN_BRACECLOSE
	sstatement ::= shead TOKEN_BRACEOPEN statements * TOKEN_BRACECLOSE
	sstatement ::= * TOKEN_BRACEOPEN sstatement_a statements TOKEN_BRACECLOSE
	shead ::= * TOKEN_SUBGRAPH text
	 " ]
 node [ id 6 label " startdot2 ::= TOKEN_BRACEOPEN statements * TOKEN_BRACECLOSE
	ctext ::= * TOKEN_QTEXT
	ctext ::= * ctext TOKEN_PLUS TOKEN_QTEXT
	text ::= * TOKEN_TEXT
	text ::= * TOKEN_NUM
	text ::= * ctext
	statements ::= statements * statement
	statement ::= * statement2
	statement ::= * statement2 TOKEN_SC
	statement2 ::= * nstatement
	statement2 ::= * estatement
	statement2 ::= * astatement
	statement2 ::= * sstatement
	nstatement ::= * nstatement_nidid oattrib
	nstatement_nidid ::= * nidid
	nidid ::= * text
	nidid ::= * text TOKEN_COLON text
	nidid ::= * text TOKEN_COLON text TOKEN_COLON text
	nid ::= * text
	nid ::= * text TOKEN_COLON text
	nid ::= * text TOKEN_COLON text TOKEN_COLON text
	estatement ::= * estatement_nid estatement_erhs oattrib
	estatement ::= * estatement_sstatement estatement_erhs oattrib
	estatement_nid ::= * nid
	estatement_sstatement ::= * sstatement
	astatement ::= * atype tattr
	astatement ::= * aset
	atype ::= * TOKEN_GRAPH
	atype ::= * TOKEN_NODE
	atype ::= * TOKEN_EDGE
	aset ::= * text TOKEN_IS text
	sstatement ::= * shead TOKEN_BRACEOPEN statements TOKEN_BRACECLOSE
	sstatement ::= * TOKEN_BRACEOPEN sstatement_a statements TOKEN_BRACECLOSE
	shead ::= * TOKEN_SUBGRAPH text
	 " ]
 node [ id 7 label " ctext ::= * TOKEN_QTEXT
	ctext ::= * ctext TOKEN_PLUS TOKEN_QTEXT
	text ::= * TOKEN_TEXT
	text ::= * TOKEN_NUM
	text ::= * ctext
	sattr ::= * text TOKEN_IS text
	sattr2 ::= * sattr
	sattr2 ::= * text
	iattr ::= * sattr2 iattr
	iattr ::= sattr2 * iattr
	iattr ::= * sattr2 TOKEN_COMMA iattr
	iattr ::= sattr2 * TOKEN_COMMA iattr
	iattr ::= *
	 " ]
 node [ id 8 label " ctext ::= * TOKEN_QTEXT
	ctext ::= * ctext TOKEN_PLUS TOKEN_QTEXT
	text ::= * TOKEN_TEXT
	text ::= * TOKEN_NUM
	text ::= * ctext
	sattr ::= * text TOKEN_IS text
	sattr2 ::= * sattr
	sattr2 ::= * text
	iattr ::= * sattr2 iattr
	iattr ::= * sattr2 TOKEN_COMMA iattr
	iattr ::= sattr2 TOKEN_COMMA * iattr
	iattr ::= *
	 " ]
 node [ id 9 label " ctext ::= * TOKEN_QTEXT
	ctext ::= * ctext TOKEN_PLUS TOKEN_QTEXT
	text ::= * TOKEN_TEXT
	text ::= * TOKEN_NUM
	text ::= * ctext
	sattr ::= * text TOKEN_IS text
	sattr2 ::= * sattr
	sattr2 ::= * text
	iattr ::= * sattr2 iattr
	iattr ::= * sattr2 TOKEN_COMMA iattr
	iattr ::= *
	tattr ::= TOKEN_BRACKETOPEN * iattr TOKEN_BRACKETCLOSE
	 " ]
 node [ id 10 label " startdot ::= utf thetype * thename startdot2
	thename ::= * text
	thename ::= *
	ctext ::= * TOKEN_QTEXT
	ctext ::= * ctext TOKEN_PLUS TOKEN_QTEXT
	text ::= * TOKEN_TEXT
	text ::= * TOKEN_NUM
	text ::= * ctext
	 " ]
 node [ id 11 label " ctext ::= * TOKEN_QTEXT
	ctext ::= * ctext TOKEN_PLUS TOKEN_QTEXT
	text ::= * TOKEN_TEXT
	text ::= * TOKEN_NUM
	text ::= * ctext
	shead ::= TOKEN_SUBGRAPH * text
	 " ]
 node [ id 12 label " ctext ::= * TOKEN_QTEXT
	ctext ::= * ctext TOKEN_PLUS TOKEN_QTEXT
	text ::= * TOKEN_TEXT
	text ::= * TOKEN_NUM
	text ::= * ctext
	aset ::= text TOKEN_IS * text
	 " ]
 node [ id 13 label " ctext ::= * TOKEN_QTEXT
	ctext ::= * ctext TOKEN_PLUS TOKEN_QTEXT
	text ::= * TOKEN_TEXT
	text ::= * TOKEN_NUM
	text ::= * ctext
	nidid ::= text TOKEN_COLON text TOKEN_COLON * text
	nid ::= text TOKEN_COLON text TOKEN_COLON * text
	 " ]
 node [ id 14 label " ctext ::= * TOKEN_QTEXT
	ctext ::= * ctext TOKEN_PLUS TOKEN_QTEXT
	text ::= * TOKEN_TEXT
	text ::= * TOKEN_NUM
	text ::= * ctext
	nidid ::= text TOKEN_COLON * text
	nidid ::= text TOKEN_COLON * text TOKEN_COLON text
	nid ::= text TOKEN_COLON * text
	nid ::= text TOKEN_COLON * text TOKEN_COLON text
	 " ]
 node [ id 15 label " ctext ::= * TOKEN_QTEXT
	ctext ::= * ctext TOKEN_PLUS TOKEN_QTEXT
	text ::= * TOKEN_TEXT
	text ::= * TOKEN_NUM
	text ::= * ctext
	sattr ::= text TOKEN_IS * text
	 " ]
 node [ id 16 label " tattr ::= * TOKEN_BRACKETOPEN iattr TOKEN_BRACKETCLOSE
	oattrib ::= * tattr oattrib
	oattrib ::= *
	estatement ::= estatement_sstatement estatement_erhs * oattrib
	 " ]
 node [ id 17 label " tattr ::= * TOKEN_BRACKETOPEN iattr TOKEN_BRACKETCLOSE
	oattrib ::= * tattr oattrib
	oattrib ::= *
	estatement ::= estatement_nid estatement_erhs * oattrib
	 " ]
 node [ id 18 label " tattr ::= * TOKEN_BRACKETOPEN iattr TOKEN_BRACKETCLOSE
	oattrib ::= * tattr oattrib
	oattrib ::= tattr * oattrib
	oattrib ::= *
	 " ]
 node [ id 19 label " nstatement ::= nstatement_nidid * oattrib
	tattr ::= * TOKEN_BRACKETOPEN iattr TOKEN_BRACKETCLOSE
	oattrib ::= * tattr oattrib
	oattrib ::= *
	 " ]
 node [ id 20 label " startdot ::= utf * thetype thename startdot2
	thetype ::= * TOKEN_STRICT TOKEN_GRAPH
	thetype ::= * TOKEN_GRAPH
	thetype ::= * TOKEN_STRICT TOKEN_DIGRAPH
	thetype ::= * TOKEN_DIGRAPH
	 " ]
 node [ id 21 label " tattr ::= * TOKEN_BRACKETOPEN iattr TOKEN_BRACKETCLOSE
	astatement ::= atype * tattr
	 " ]
 node [ id 22 label " startdot2 ::= * TOKEN_BRACEOPEN statements TOKEN_BRACECLOSE
	startdot ::= utf thetype thename * startdot2
	 " ]
 node [ id 23 label " sstatement_a ::= *
	sstatement ::= TOKEN_BRACEOPEN * sstatement_a statements TOKEN_BRACECLOSE
	 " ]
 node [ id 24 label " estatement ::= estatement_sstatement * estatement_erhs oattrib
	estatement_erhs ::= *
	 " ]
 node [ id 25 label " estatement ::= estatement_nid * estatement_erhs oattrib
	estatement_erhs ::= *
	 " ]
 node [ id 26 label " thetype ::= TOKEN_STRICT * TOKEN_GRAPH
	thetype ::= TOKEN_STRICT * TOKEN_DIGRAPH
	 " ]
 node [ id 27 label " nidid ::= text *
	nidid ::= text * TOKEN_COLON text
	nidid ::= text * TOKEN_COLON text TOKEN_COLON text
	nid ::= text *
	nid ::= text * TOKEN_COLON text
	nid ::= text * TOKEN_COLON text TOKEN_COLON text
	aset ::= text * TOKEN_IS text
	 " ]
 node [ id 28 label " startdot ::= utf thetype thename startdot2 *
	 " ]
 node [ id 29 label " sstatement ::= shead * TOKEN_BRACEOPEN statements TOKEN_BRACECLOSE
	 " ]
 node [ id 30 label " nidid ::= text TOKEN_COLON text *
	nidid ::= text TOKEN_COLON text * TOKEN_COLON text
	nid ::= text TOKEN_COLON text *
	nid ::= text TOKEN_COLON text * TOKEN_COLON text
	 " ]
 node [ id 31 label " tattr ::= TOKEN_BRACKETOPEN iattr * TOKEN_BRACKETCLOSE
	 " ]
 node [ id 32 label " sattr ::= text * TOKEN_IS text
	sattr2 ::= text *
	 " ]
 node [ id 33 label " statement2 ::= sstatement *
	estatement_sstatement ::= sstatement *
	 " ]
 node [ id 34 label " statement ::= statement2 *
	statement ::= statement2 * TOKEN_SC
	 " ]
 node [ id 35 label " ctext ::= ctext TOKEN_PLUS * TOKEN_QTEXT
	 " ]
 node [ id 36 label " ctext ::= ctext * TOKEN_PLUS TOKEN_QTEXT
	text ::= ctext *
	 " ]
 node [ id 37 label " thetype ::= TOKEN_DIGRAPH *
	 " ]
 node [ id 38 label " thetype ::= TOKEN_GRAPH *
	 " ]
 node [ id 39 label " thetype ::= TOKEN_STRICT TOKEN_DIGRAPH *
	 " ]
 node [ id 40 label " thetype ::= TOKEN_STRICT TOKEN_GRAPH *
	 " ]
 node [ id 41 label " thename ::= text *
	 " ]
 node [ id 42 label " shead ::= TOKEN_SUBGRAPH text *
	 " ]
 node [ id 43 label " sstatement ::= TOKEN_BRACEOPEN sstatement_a statements TOKEN_BRACECLOSE *
	 " ]
 node [ id 44 label " sstatement ::= shead TOKEN_BRACEOPEN statements TOKEN_BRACECLOSE *
	 " ]
 node [ id 45 label " statements ::= statement *
	 " ]
 node [ id 46 label " atype ::= TOKEN_EDGE *
	 " ]
 node [ id 47 label " atype ::= TOKEN_NODE *
	 " ]
 node [ id 48 label " atype ::= TOKEN_GRAPH *
	 " ]
 node [ id 49 label " astatement ::= aset *
	 " ]
 node [ id 50 label " astatement ::= atype tattr *
	 " ]
 node [ id 51 label " estatement_nid ::= nid *
	 " ]
 node [ id 52 label " estatement ::= estatement_sstatement estatement_erhs oattrib *
	 " ]
 node [ id 53 label " estatement ::= estatement_nid estatement_erhs oattrib *
	 " ]
 node [ id 54 label " aset ::= text TOKEN_IS text *
	 " ]
 node [ id 55 label " nidid ::= text TOKEN_COLON text TOKEN_COLON text *
	nid ::= text TOKEN_COLON text TOKEN_COLON text *
	 " ]
 node [ id 56 label " nstatement_nidid ::= nidid *
	 " ]
 node [ id 57 label " oattrib ::= tattr oattrib *
	 " ]
 node [ id 58 label " tattr ::= TOKEN_BRACKETOPEN iattr TOKEN_BRACKETCLOSE *
	 " ]
 node [ id 59 label " iattr ::= sattr2 TOKEN_COMMA iattr *
	 " ]
 node [ id 60 label " iattr ::= sattr2 iattr *
	 " ]
 node [ id 61 label " sattr2 ::= sattr *
	 " ]
 node [ id 62 label " sattr ::= text TOKEN_IS text *
	 " ]
 node [ id 63 label " nstatement ::= nstatement_nidid oattrib *
	 " ]
 node [ id 64 label " statement2 ::= astatement *
	 " ]
 node [ id 65 label " statement2 ::= estatement *
	 " ]
 node [ id 66 label " statement2 ::= nstatement *
	 " ]
 node [ id 67 label " statement ::= statement2 TOKEN_SC *
	 " ]
 node [ id 68 label " statements ::= statements statement *
	 " ]
 node [ id 69 label " text ::= TOKEN_NUM *
	 " ]
 node [ id 70 label " text ::= TOKEN_TEXT *
	 " ]
 node [ id 71 label " ctext ::= ctext TOKEN_PLUS TOKEN_QTEXT *
	 " ]
 node [ id 72 label " ctext ::= TOKEN_QTEXT *
	 " ]
 node [ id 73 label " startdot2 ::= TOKEN_BRACEOPEN statements TOKEN_BRACECLOSE *
	 " ]
 node [ id 74 label " utf ::= TOKEN_UTF8BOM *
	 " ]
 edge [ source 0 target 20 label "utf" ]
 edge [ source 1 target 23 label "TOKEN_BRACEOPEN" ]
 edge [ source 1 target 11 label "TOKEN_SUBGRAPH" ]
 edge [ source 1 target 27 label "text" ]
 edge [ source 1 target 36 label "ctext" ]
 edge [ source 1 target 33 label "sstatement" ]
 edge [ source 1 target 4 label "statements" ]
 edge [ source 1 target 4 label "statement" ]
 edge [ source 1 target 34 label "statement2" ]
 edge [ source 1 target 34 label "nstatement" ]
 edge [ source 1 target 34 label "estatement" ]
 edge [ source 1 target 34 label "astatement" ]
 edge [ source 1 target 19 label "nstatement_nidid" ]
 edge [ source 1 target 25 label "estatement_nid" ]
 edge [ source 1 target 24 label "estatement_sstatement" ]
 edge [ source 1 target 21 label "atype" ]
 edge [ source 1 target 29 label "shead" ]
 edge [ source 2 target 23 label "TOKEN_BRACEOPEN" ]
 edge [ source 2 target 11 label "TOKEN_SUBGRAPH" ]
 edge [ source 2 target 27 label "text" ]
 edge [ source 2 target 36 label "ctext" ]
 edge [ source 2 target 33 label "sstatement" ]
 edge [ source 2 target 5 label "statements" ]
 edge [ source 2 target 5 label "statement" ]
 edge [ source 2 target 34 label "statement2" ]
 edge [ source 2 target 34 label "nstatement" ]
 edge [ source 2 target 34 label "estatement" ]
 edge [ source 2 target 34 label "astatement" ]
 edge [ source 2 target 19 label "nstatement_nidid" ]
 edge [ source 2 target 25 label "estatement_nid" ]
 edge [ source 2 target 24 label "estatement_sstatement" ]
 edge [ source 2 target 21 label "atype" ]
 edge [ source 2 target 29 label "shead" ]
 edge [ source 3 target 23 label "TOKEN_BRACEOPEN" ]
 edge [ source 3 target 11 label "TOKEN_SUBGRAPH" ]
 edge [ source 3 target 27 label "text" ]
 edge [ source 3 target 36 label "ctext" ]
 edge [ source 3 target 33 label "sstatement" ]
 edge [ source 3 target 6 label "statements" ]
 edge [ source 3 target 6 label "statement" ]
 edge [ source 3 target 34 label "statement2" ]
 edge [ source 3 target 34 label "nstatement" ]
 edge [ source 3 target 34 label "estatement" ]
 edge [ source 3 target 34 label "astatement" ]
 edge [ source 3 target 19 label "nstatement_nidid" ]
 edge [ source 3 target 25 label "estatement_nid" ]
 edge [ source 3 target 24 label "estatement_sstatement" ]
 edge [ source 3 target 21 label "atype" ]
 edge [ source 3 target 29 label "shead" ]
 edge [ source 4 target 23 label "TOKEN_BRACEOPEN" ]
 edge [ source 4 target 11 label "TOKEN_SUBGRAPH" ]
 edge [ source 4 target 27 label "text" ]
 edge [ source 4 target 36 label "ctext" ]
 edge [ source 4 target 33 label "sstatement" ]
 edge [ source 4 target 34 label "statement2" ]
 edge [ source 4 target 34 label "nstatement" ]
 edge [ source 4 target 34 label "estatement" ]
 edge [ source 4 target 34 label "astatement" ]
 edge [ source 4 target 19 label "nstatement_nidid" ]
 edge [ source 4 target 25 label "estatement_nid" ]
 edge [ source 4 target 24 label "estatement_sstatement" ]
 edge [ source 4 target 21 label "atype" ]
 edge [ source 4 target 29 label "shead" ]
 edge [ source 5 target 23 label "TOKEN_BRACEOPEN" ]
 edge [ source 5 target 11 label "TOKEN_SUBGRAPH" ]
 edge [ source 5 target 27 label "text" ]
 edge [ source 5 target 36 label "ctext" ]
 edge [ source 5 target 33 label "sstatement" ]
 edge [ source 5 target 34 label "statement2" ]
 edge [ source 5 target 34 label "nstatement" ]
 edge [ source 5 target 34 label "estatement" ]
 edge [ source 5 target 34 label "astatement" ]
 edge [ source 5 target 19 label "nstatement_nidid" ]
 edge [ source 5 target 25 label "estatement_nid" ]
 edge [ source 5 target 24 label "estatement_sstatement" ]
 edge [ source 5 target 21 label "atype" ]
 edge [ source 5 target 29 label "shead" ]
 edge [ source 6 target 23 label "TOKEN_BRACEOPEN" ]
 edge [ source 6 target 11 label "TOKEN_SUBGRAPH" ]
 edge [ source 6 target 27 label "text" ]
 edge [ source 6 target 36 label "ctext" ]
 edge [ source 6 target 33 label "sstatement" ]
 edge [ source 6 target 34 label "statement2" ]
 edge [ source 6 target 34 label "nstatement" ]
 edge [ source 6 target 34 label "estatement" ]
 edge [ source 6 target 34 label "astatement" ]
 edge [ source 6 target 19 label "nstatement_nidid" ]
 edge [ source 6 target 25 label "estatement_nid" ]
 edge [ source 6 target 24 label "estatement_sstatement" ]
 edge [ source 6 target 21 label "atype" ]
 edge [ source 6 target 29 label "shead" ]
 edge [ source 7 target 8 label "TOKEN_COMMA" ]
 edge [ source 7 target 32 label "text" ]
 edge [ source 7 target 36 label "ctext" ]
 edge [ source 7 target 7 label "sattr" ]
 edge [ source 7 target 7 label "sattr2" ]
 edge [ source 8 target 32 label "text" ]
 edge [ source 8 target 36 label "ctext" ]
 edge [ source 8 target 7 label "sattr" ]
 edge [ source 8 target 7 label "sattr2" ]
 edge [ source 9 target 32 label "text" ]
 edge [ source 9 target 36 label "ctext" ]
 edge [ source 9 target 7 label "sattr" ]
 edge [ source 9 target 7 label "sattr2" ]
 edge [ source 9 target 31 label "iattr" ]
 edge [ source 10 target 36 label "ctext" ]
 edge [ source 10 target 22 label "thename" ]
 edge [ source 11 target 36 label "ctext" ]
 edge [ source 12 target 36 label "ctext" ]
 edge [ source 13 target 36 label "ctext" ]
 edge [ source 14 target 30 label "text" ]
 edge [ source 14 target 36 label "ctext" ]
 edge [ source 15 target 36 label "ctext" ]
 edge [ source 16 target 9 label "TOKEN_BRACKETOPEN" ]
 edge [ source 16 target 18 label "tattr" ]
 edge [ source 17 target 9 label "TOKEN_BRACKETOPEN" ]
 edge [ source 17 target 18 label "tattr" ]
 edge [ source 18 target 9 label "TOKEN_BRACKETOPEN" ]
 edge [ source 18 target 18 label "tattr" ]
 edge [ source 19 target 9 label "TOKEN_BRACKETOPEN" ]
 edge [ source 19 target 18 label "tattr" ]
 edge [ source 20 target 26 label "TOKEN_STRICT" ]
 edge [ source 20 target 10 label "thetype" ]
 edge [ source 21 target 9 label "TOKEN_BRACKETOPEN" ]
 edge [ source 22 target 3 label "TOKEN_BRACEOPEN" ]
 edge [ source 22 target 28 label "startdot2" ]
 edge [ source 23 target 1 label "sstatement_a" ]
 edge [ source 24 target 16 label "estatement_erhs" ]
 edge [ source 25 target 17 label "estatement_erhs" ]
 edge [ source 27 target 14 label "TOKEN_COLON" ]
 edge [ source 27 target 12 label "TOKEN_IS" ]
 edge [ source 29 target 2 label "TOKEN_BRACEOPEN" ]
 edge [ source 30 target 13 label "TOKEN_COLON" ]
 edge [ source 32 target 15 label "TOKEN_IS" ]
 edge [ source 36 target 35 label "TOKEN_PLUS" ]

]
