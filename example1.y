%include {
#include <stdio.h>
#include <stdlib.h>
#include "example1.h"
}

%token_type {int}

%left PLUS. 
%left MUL.

%syntax_error {
  printf ("Syntax error!\n");
}

program ::= stdelimit exprlist(A) enddelimit. { printf ("Result=%d\n", A); }
stdelimit ::= ST_DELIMIT.  
enddelimit ::= END_DELIMIT.

exprlist(A) ::= expr(B).    {A=B;}

expr(A) ::= expr(B) PLUS   expr(C).	{ A = B + C; }
expr(A) ::= expr(B) MUL  expr(C).	{ A = B * C; } 
expr(A) ::= number(B).			{ A = B; }


number(A) ::= INTEGER(B).		{ A = B; }
number(A) ::= FLOAT(B).			{ A = B; }



